package ransack

import (
	"net/http"
	"strconv"

	"github.com/jinzhu/gorm"
)

func sort(db *gorm.DB, r *http.Request) *gorm.DB {
	sorts, ok := r.URL.Query()["sort"]
	if !ok {
		sorts, ok = r.URL.Query()["sort[]"]
		if !ok {
			return db
		}
	}
	s := db
	for _, sort := range sorts {
		switch sort[0] {
		case '+':
			sort = sort[1:]
			break
		case '-':
			sort = sort[1:] + " DESC"
			break
		}
		s = s.Order(sort)
	}
	return s
}

func paginate(db *gorm.DB, r *http.Request) *gorm.DB {
	pageS := r.URL.Query().Get("page")
	if pageS == "" {
		pageS = "1"
	}
	perPageS := r.URL.Query().Get("per_page")
	if perPageS == "" {
		perPageS = "20"
	}
	page, err := strconv.ParseInt(pageS, 10, 64)
	if err != nil {
		page = 1
	}
	perPage, err := strconv.ParseInt(perPageS, 10, 64)
	if err != nil {
		perPage = 20
	}
	if perPage <= 0 || perPage >= 50 {
		perPage = 20
	}
	if page <= 0 {
		page = 1
	}
	return db.Limit(perPage).Offset((page - 1) * perPage)
}
