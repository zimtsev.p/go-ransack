package ransack

import (
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
)

type Configuration struct {
	Models      []Model
	AuthFlag    bool
	AuthRow     map[string]string
	CurrentUser func(*gin.Context) interface{}
	DB          *gorm.DB
}

type Model interface {
	MakeResponse(int, *gorm.DB, *gin.Context)
}

func Authorize(scope *gorm.DB, cuID int64, sqlRow string) {
	if cuID == 0 {
		*scope = *scope.Where("1 = 0")
		return
	}
	*scope = *scope.Where(sqlRow, cuID)
}
