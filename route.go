package ransack

import "github.com/gin-gonic/gin"

func APIRoutes(group *gin.RouterGroup) *gin.RouterGroup {
	for url, handler := range Index {
		group.GET(url, handler)
	}
	return group
}

//2 version
// func (group *gin.RouterGroup) APIRoutes() {
// 	for url, handler := range Index {
// 		group.GET(url, handler)
// 	}
// }
