package ransack

import (
	"net/http"
	"reflect"
	"strings"

	"github.com/gin-gonic/gin"
)

var Index map[string]func(ctx *gin.Context)
var Config Configuration

func Init() {
	if len(Config.Models) > 0 {
		Index = generateApiIndexMap(Config.Models...)
	}
}

func generateApiIndexMap(structs ...Model) map[string]func(ctx *gin.Context) {
	ret := map[string]func(ctx *gin.Context){}
	skips := map[string]struct{}{}
	preloadType := map[string]string{}
	for _, modelT := range structs {
		model := modelT
		elem := reflect.TypeOf(model).Elem()
		modelName := elem.PkgPath() + "." + elem.Name()
		scope := Config.DB.NewScope(model)
		types := map[string]string{}
		for _, field := range scope.GetStructFields() {
			fieldKey := modelName + "::" + field.Name
			settings := field.Struct.Tag.Get("api")
			if settings == "-" {
				skips[fieldKey] = struct{}{}
				continue
			}
			if field.Relationship != nil {
				types[field.Name] = "preload"
				preloadType[fieldKey] = getPreloadType(field.Struct.Type)
				continue
			}
			types[field.DBName] = getType(field.Struct.Type)
		}
		ret["/"+scope.TableName()] = func(ctx *gin.Context) {
			q := ctx.Request.URL.Query()
			scope := Config.DB.Model(model)
			if Config.AuthFlag && Config.AuthRow[reflect.TypeOf(model).Elem().Name()] != "" {
				val := reflect.ValueOf(Config.CurrentUser(ctx)).FieldByName("ID")
				Authorize(scope, val.Int(), Config.AuthRow[reflect.TypeOf(model).Elem().Name()])
			}
			for _, key := range []string{"includes", "includes[]"} {
				if values, ok := q[key]; ok {
					for _, value := range values {
						lastModel := modelName
						ar := strings.Split(value, ".")
						schema := []string{}
						for _, fn := range ar {
							key := lastModel + "::" + fn
							//spew.Dump(skips)
							if _, skip := skips[key]; skip {
								break
							}
							schema = append(schema, fn)
							lastModel = preloadType[key]
						}
						if len(schema) != 0 {
							scope = scope.Preload(strings.Join(schema, "."))
						}
					}
				}
			}
			for key, values := range q {
				for field, kind := range types {
					field = strings.TrimSuffix(field, "[]")
					if strings.HasPrefix(key, field) {
						switch kind {
						case "string":
							scope = doString(scope, key, values)
							break
						case "int":
							scope = doInt(scope, key, values)
							break
						case "time":
							scope = doTime(scope, key, values)
							break
						}
					}
				}
			}
			c := 0
			if err := scope.Count(&c).Error; err != nil {
				ctx.JSON(http.StatusBadRequest, err.Error())
				return
			}
			scope = sort(scope, ctx.Request)
			scope = paginate(scope, ctx.Request)
			model.MakeResponse(c, scope, ctx)
		}

	}
	return ret
}
