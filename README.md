# go-ransack

go-ransack
## Конфигурация
### Конфигурация происходит в структуре Configuration. Задается массив моделей для обработки:
```
Models      []Model
```
### Для работы необходимо добавить функцию MakeResponse в каждую модель. Пример реализация для модели User:
```
func (u *User) MakeResponse(total int, scope *gorm.DB, ctx *gin.Context) {
	array := []User{}
	if err := scope.Find(&array).Error; err != nil {
		ctx.JSON(http.StatusBadRequest, err.Error())
		return
	}
	ctx.JSON(http.StatusOK, gin.H{
		"total": total,
		"data":  array,
	})
	return
}
```
### Для любой другой необходимо заменить `(u *User)` на свою модели и изменить  `array := []User{}` на свою модель.
### Подключение базы данных через gorm
```
DB          *gorm.DB
```
### Структура конфигурации находится в файле config.go
### Пример использования, конфигурация  в функции init() main пакета
```
var config *ransack.Configuration
func init(){
	config = &ransack.Configuration{}
		config.Models = []ransack.Model{}
		config.Models = append(config.Models, &models.User{}, &models.Book{}, &models.Recomendation{})
		config.DB = models.DB
		ransack.Config = *config
		ransack.Init()
}
```
### Пример формирования роутов для gin
```
r := gin.Default()
testRoutes := r.Group("/v1")
ransack.APIRoutes(testRoutes)
```
## Авторизация.
### AuthFlag - отвечает за необходимость авторизации. AuthRow - передает названия моделей, для которых необходима авторизация и строку для sql запроса. Реализуется как "Имя модели":"sql часть для модели"
```
	AuthFlag    bool
	AuthRow     map[string]string
```
