package ransack

import "reflect"

func getType(t reflect.Type) string {
	switch t.Kind() {
	case reflect.Int64:
		return "int"
	case reflect.String:
		return "string"
	case reflect.Ptr:
		return getType(t.Elem())
	case reflect.Struct:
		if t.PkgPath() == "time" && t.Name() == "Time" {
			return "time"
		}
	}
	return ""
}

func getPreloadType(t reflect.Type) string {
	switch t.Kind() {
	case reflect.Slice:
		return getPreloadType(t.Elem())
	case reflect.Ptr:
		return getPreloadType(t.Elem())
	default:
		return t.PkgPath() + "." + t.Name()
	}
}
