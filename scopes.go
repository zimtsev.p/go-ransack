package ransack

import (
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/jinzhu/gorm"
)

func doTime(db *gorm.DB, key string, values []string) *gorm.DB {
	value, err := time.Parse("02.01.2006 15:04", strings.ToUpper(values[0])) //time.RFC3339
	if err != nil {
		//spew.Dump(err)
		return db.Where("1 = 0")
	}
	if strings.HasSuffix(key, "_gteq_datetime") {
		field := strings.TrimSuffix(key, "_gteq_datetime")
		return db.Where(fmt.Sprintf("%v > ?", field), value)
	} else if strings.HasSuffix(key, "_lteq_datetime") {
		field := strings.TrimSuffix(key, "_lteq_datetime")
		return db.Where(fmt.Sprintf("%v < ?", field), value)
	}
	return db
}

func doInt(db *gorm.DB, key string, values []string) *gorm.DB {
	value, err := strconv.ParseInt(values[0], 10, 64)
	if err != nil {
		return db.Where("1 = 0")
	}
	if strings.HasSuffix(key, "_eq") {
		field := strings.TrimSuffix(key, "_eq")
		return db.Where(fmt.Sprintf("%v = ?", field), value)
	} else if strings.HasSuffix(key, "_equals") {
		field := strings.TrimSuffix(key, "_equals")
		return db.Where(fmt.Sprintf("%v = ?", field), value)
	} else if strings.HasSuffix(key, "_greater_than") {
		field := strings.TrimSuffix(key, "_greater_than")
		return db.Where(fmt.Sprintf("%v > ?", field), value)
	} else if strings.HasSuffix(key, "_less_than") {
		field := strings.TrimSuffix(key, "_less_than")
		return db.Where(fmt.Sprintf("%v < ?", field), value)
	}
	return db
}

func doString(db *gorm.DB, key string, values []string) *gorm.DB {
	if strings.HasSuffix(key, "_equals") {
		field := strings.TrimSuffix(key, "_equals")
		return db.Where(fmt.Sprintf("%v ILIKE ?", field), values[0])
	} else if strings.HasSuffix(key, "_eq") {
		field := strings.TrimSuffix(key, "_eq")
		return db.Where(fmt.Sprintf("%v ILIKE ?", field), values[0])
	} else if strings.HasSuffix(key, "_starts_with") {
		field := strings.TrimSuffix(key, "_starts_with")
		return db.Where(fmt.Sprintf("%v ILIKE ?", field), values[0]+"%")
	} else if strings.HasSuffix(key, "_ends_with") {
		field := strings.TrimSuffix(key, "_ends_with")
		return db.Where(fmt.Sprintf("%v ILIKE ?", field), "%"+values[0])
	} else if strings.HasSuffix(key, "_contains") {
		field := strings.TrimSuffix(key, "_contains")
		return db.Where(fmt.Sprintf("%v ILIKE ?", field), "%"+values[0]+"%")
	}
	return db
}
